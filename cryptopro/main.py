from encryption import *
from flask import Flask, request
import os


def make_app():
    app = Flask(__name__)

    @app.errorhandler(404)
    def page_not_found(e):
        return 404

    @app.route('/signature/')
    def signature():
        thumbprint = request.args.get('thumbprint')
        message = request.args.get('message')
        if message is not None and thumbprint is not None:
            return signature_base64(message, thumbprint)
        else:
            return {'error': 1, 'message': 'One or more required arguments are empty'}

    @app.route('/signature_xml')
    def signature_xml():
        thumbprint = request.args.get('thumbprint')
        puth_name = request.args.get('puth_name')
        file_name = request.args.get('file_name')
        prefix = request.args.get('prefix', '')
        idd = request.args.get('idd')
        hash = request.args.get('hash')
        if puth_name is not None and thumbprint is not None and hash is not None:
            return signature_xml_base64(puth_name, file_name, thumbprint, hash, idd, prefix)
        else:
            return {'error': 1, 'message': 'thumbprint is empty'}

    return app


if __name__ == '__main__':
    host = os.getenv('IP', '0.0.0.0')
    # host = os.getenv('IP', '127.0.0.1')
    port = int(os.getenv('PORT', 80))
    app = make_app()
    app.run(host=host, port=port, debug=True)
