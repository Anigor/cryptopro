import os
import sys
import time
import hashlib
from config import Configuration
from ftplib import FTP_TLS
sys.path.append(r'/home/a-asu/pycades_0.1.30636/')
import pycades
import pymysql


def save_xml_to_db(idd, xml, prefix):
    con = pymysql.connect(host=Configuration.DB.host, user=Configuration.DB.user, password=Configuration.DB.password, database=Configuration.DB.database)

    with con:
        cur = con.cursor()
        cur.execute(f"UPDATE `modx_mss_semd` SET `{prefix}signature`='{xml}' WHERE  `mss_id` = {idd}")
        con.commit()

def signature_base64(text_to_encrypt, thumbprint):
    """Создани открепленной подписи"""
    name_file = str(int(time.time()))
    puth_name_file = '/tmp/'+name_file
    puth_name_sert_file = '/tmp/'+name_file+'.sgn'
    f = open(puth_name_file, 'w+')
    f.write(text_to_encrypt)
    f.close()

    command = 'sudo /opt/cprocsp/bin/amd64/cryptcp -signf -dir /tmp/ -strict -cert -detached -thumbprint {0} /tmp/{1}'.format(
        thumbprint, name_file)
    store = pycades.Store()
    store.Open(pycades.CADESCOM_CONTAINER_STORE, pycades.CAPICOM_MY_STORE, pycades.CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED)
    certs = store.Certificates

    if certs.Count == 0:
        return {'error': 1, 'message': 'Certificates empty'}

    for index in range(1, certs.Count+1):
        cert = certs.Item(index)
        certificate_status = cert.IsValid()
        if certificate_status.Result:
            if cert.Thumbprint == thumbprint.upper():
                os.system(command)
                f = open(puth_name_sert_file, 'r')
                response_signature = f.read()
                f.close()
                os.remove(puth_name_file)
                os.remove(puth_name_sert_file)
                return response_signature
                # signer = pycades.Signer()
                # signer.Certificate = cert
                # signer.CheckCertificate = True
                # signed_data = pycades.SignedData()
                # signed_data.Content = text_to_encrypt
                # signature = signed_data.SignCades(signer, pycades.CADESCOM_CADES_BES)
                # response_signature = signature.replace('+', '-').replace('/', '_').replace('=', '')
                # return {'error': 0, 'message': response_signature}

    os.remove(puth_name_file)

    return {'error': 1, 'message': 'The requested certificate was not found'}

def signature_xml_base64(path_name, file_name, thumbprint, hash, idd, prefix):
    store = pycades.Store()
    store.Open(pycades.CADESCOM_CONTAINER_STORE, pycades.CAPICOM_MY_STORE, pycades.CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED)
    certs = store.Certificates

    if certs.Count == 0:
        return {'error': 1, 'message': 'Certificates empty'}

    for index in range(1, certs.Count + 1):
        cert = certs.Item(index)
        certificate_status = cert.IsValid()
        if certificate_status.Result:
            if cert.Thumbprint == thumbprint.upper():
                print('cert find')
                path_file_local = Configuration.PATH_FILE + str(int(time.time()))
                path_file_local_ser = Configuration.PATH_FILE + "ser" + str(int(time.time()))
                ftp_path = path_name + file_name
                ftp_path_sing = path_name + "sing_" + file_name

                signer = pycades.Signer()
                signer.Certificate = cert
                signer.CheckCertificate = True

                try:
                    content_to_sign = ""
                    hash_local = ""
                    number_incorrect_attempts = 0
                    while hash_local != hash:
                        with FTP_TLS(Configuration.FTP_HOST) as ftp:
                            ftp.sendcmd('USER ' + Configuration.FTP_LOGIN)
                            ftp.sendcmd('PASS ' + Configuration.FTP_PASS)
                            my_file = open(path_file_local, 'wb')
                            ftp.retrbinary('RETR ' + ftp_path, my_file.write)
                        with open(path_file_local, 'r', encoding='utf-8') as fp:
                            content_to_sign = fp.read()
                        hash_local = hashlib.md5(content_to_sign.encode('utf-8')).hexdigest()
                        if hash_local != hash:
                            time.sleep(1)
                            if os.path.exists(path_file_local):
                                os.remove(path_file_local)
                            number_incorrect_attempts += 1
                        if number_incorrect_attempts > 50:
                            return {'error': 1, 'message': "Error download file", 'hash': hash_local}

                    signedXML = pycades.SignedXML()
                    signedXML.Content = content_to_sign
                    signedXML.SignatureType = pycades.CADESCOM_XML_SIGNATURE_TYPE_ENVELOPED | pycades.CADESCOM_XADES_BES
                    signature = signedXML.Sign(signer, 'Header')

                    f = open(path_file_local_ser, 'w')
                    f.write(signature)
                    f.close()
                    print(ftp_path_sing)
                    with FTP_TLS(Configuration.FTP_HOST) as ftp:
                        ftp.sendcmd('USER ' + Configuration.FTP_LOGIN)
                        ftp.sendcmd('PASS ' + Configuration.FTP_PASS)
                        ftp.cwd('MSS-SEMD')
                        f = open(path_file_local_ser, "rb")
                        send = ftp.storbinary("STOR " + ftp_path_sing, f)
                    save_xml_to_db(idd, signature, prefix)
                    return {'error': 0, 'message': "sing correct"}
                except Exception as e:
                    print(e.message)
                    return {'error': 1, 'message': f"Error sing ({e.message})"}
                finally:
                    if os.path.exists(path_file_local):
                        os.remove(path_file_local)
                    if os.path.exists(path_file_local_ser):
                        os.remove(path_file_local_ser)
    return {'error': 1, 'message': "sing not find"}
